jVectorMap地图插件只要浏览器技术JavaScript、CSS,HTML,SVG或VML就可以浏览使用，不需要Flash或其他专有的浏览 器插件。所以jVectorMap在所有现代移动浏览器上也能正常运行。

 

官方网站：

http://jvectormap.com/

 

1、需要引入的脚本

     <link href="jvectormap/jquery.vector-map.css" media="screen" rel="stylesheet" type="text/css" />
     <script src="jvectormap/jquery-1.6.min.js" type="text/javascript"></script>
     <script src="jvectormap/jquery.vector-map.js" type="text/javascript"></script>
     <script src="jvectormap/china-zh.js" type="text/javascript"></script>
2、前端显示片段


<script type="text/javascript">
        $(function () {
            /*id为中国省份标识符，name为对应省份名称，两者固定。value为活动情况，
　　url为活动列表页面地址，数据可后台动态生成，这两项有活动的地区填写，无则留空。*/
            /*var dataStatus =
            [{ id: 'HKG', name: '香港：', value: '6', url: '#' },
            { id: 'HAI', name: '海南：', value: '0', url: '#' },
            { id: 'YUN', name: '云南：', value: '0', url: '#' },
            { id: 'BEJ', name: '北京：', value: '3', url: '#' },
            { id: 'TAJ', name: '天津：', value: '0', url: '#' },
            { id: 'XIN', name: '新疆：', value: '0', url: '#' },
            { id: 'TIB', name: '西藏：', value: '0', url: '#' },
            { id: 'QIH', name: '青海：', value: '0', url: '#' },
            { id: 'GAN', name: '甘肃：', value: '0', url: '#' },
            { id: 'NMG', name: '内蒙古：', value: '0', url: '#' },
            { id: 'NXA', name: '宁夏：', value: '0', url: '#' },
            { id: 'SHX', name: '山西：', value: '0', url: '#' },
            { id: 'LIA', name: '辽宁：', value: '0', url: '#' },
            { id: 'JIL', name: '吉林：', value: '0', url: '#' },
            { id: 'HLJ', name: '黑龙江：', value: '0', url: '#' },
            { id: 'HEB', name: '河北：', value: '0', url: '#' },
            { id: 'SHD', name: '山东：', value: '0', url: '#' },
            { id: 'HEN', name: '河南：', value: '0', url: '#' },
            { id: 'SHA', name: '陕西：', value: '0', url: '#' },
            { id: 'SCH', name: '四川：', value: '1', url: '#' },
            { id: 'CHQ', name: '重庆：', value: '0', url: '#' },
            { id: 'HUB', name: '湖北：', value: '1', url: '#' },
            { id: 'ANH', name: '安徽：', value: '0', url: '#' },
            { id: 'JSU', name: '江苏：', value: '0', url: '#' },
            { id: 'SHH', name: '上海：', value: '0', url: '#' },
            { id: 'ZHJ', name: '浙江：', value: '0', url: '#' },
            { id: 'FUJ', name: '福建：', value: '0', url: '#' },
            { id: 'TAI', name: '台湾：', value: '0', url: '#' },
            { id: 'JXI', name: '江西：', value: '0', url: '#' },
            { id: 'HUN', name: '湖南：', value: '5', url: '#' },
            { id: 'GUI', name: '贵州：', value: '0', url: '#' },
            { id: 'GXI', name: '广西：', value: '0', url: '#' },
            { id: 'GUD', name: '广东：', value: '9', url: '#'}];*/
            var dataStatus = @Html.Raw(ViewData["mapDataJson"].ToString());
$('#map').vectorMap({
                map: 'china_zh',
                backgroundColor: false,
                color: "#BBBBBB",
                hoverColor: false,
                //显示各地区名称和活动
                onLabelShow: function (value, label, code) {
                    $.each(dataStatus, function (i, items) {
                        if (code == items.id) {
                            label.html(items.name + items.value);
                        }
                    });
                },
                //鼠标移入省份区域，改变鼠标状态
                onRegionOver: function (value, code) {
                    $.each(dataStatus, function (i, items) {
                        if ((code == items.id) && (items.value != '')) {
                            $('#map').css({ cursor: 'pointer' });
                            var josnStr = "{" + items.id + ":'#00FF00'}";
                            $('#map').vectorMap('set', 'colors', eval('(' + josnStr + ')'));
                        }
                    });
                },
                //鼠标移出省份区域，改回鼠标状态
                onRegionOut: function (value, code) {
                    $.each(dataStatus, function (i, items) {
                        if ((code == items.id) && (items.value != '')) {
                            $('#map').css({ cursor: 'auto' });

                            if (items.value != '0') {
                                var josnStr = "{" + items.id + ":'#001F76'}";
                                $('#map').vectorMap('set', 'colors', eval('(' + josnStr + ')'));
                                $('#jvectormap1_'+items.id).attr("fill-opacity",(items.opacity));
                            }
                            else {
                                var josnStr = "{" + items.id + ":'#BBBBBB'}";
                                $('#map').vectorMap('set', 'colors', eval('(' + josnStr + ')'));
                            }
                        }
                    });
                },
                //点击有活动的省份区域，打开对应活动列表页面
                onRegionClick: function (value, code) {
                    $.each(dataStatus, function (i, items) {
                        if ((code == items.id) && (items.value != '')) {
                          window.location.href = items.url;
                        }
                    });
                }
            });
            //改变有活动省份区域的颜色
            $.each(dataStatus, function (i, items) {
                if (items.value != '0') {
                    var josnStr = "{" + items.id + ":'#001F76'}";
                    $('#map').vectorMap('set', 'colors', eval('(' + josnStr + ')'));
                    $('#jvectormap1_'+items.id).attr("fill-opacity",(items.opacity));
                }
            });
        });
    </script>
</head>
<body>
    <div id="map" style="margin-left: 45px; padding-top: 10px; padding-left: 10px; background: white;
        width: 650px; height: 530px;">
    </div>
    <div style="width: 420px; height: 15px; margin: 0 auto; filter: progid:DXImageTransform.Microsoft.Gradient(startColorStr='#BBBBBB',endColorStr='#001F76',gradientType='1');
        background: -moz-linear-gradient(left, #BBBBBB, #001F76); background: -o-linear-gradient(left,#BBBBBB, #001F76);
        background: -webkit-gradient(linear, 0% 0%, 100% 0%, from(#BBBBBB), to(#001F76));
        position: relative;">
        <div style="position: absolute; top: -18px;">
            0
        </div>
        <div style="position: absolute; top: -18px; right: 0;">
            @ViewData["maxAreaCount"]
        </div>
    </div>
</body>

3、控制器数据绑定片段


复制代码
public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            //json转换
            JavaScriptSerializer _ser = new JavaScriptSerializer();

            Dictionary<string, string> _mapDataDic = new Dictionary<string, string>();
            List<YcJVectorMap.Model.Map.data> _mapDataList = this.GetMapDefault(out _mapDataDic);
            decimal maxAreaCount = 0;

            //TODO:更改数值方法1
            _mapDataList.Find(_ => _.id == "HKG").value = "26";
            _mapDataList.Find(_ => _.id == "MAC").value = "6";
            _mapDataList.Find(_ => _.id == "GUD").value = "68";

            //TODO:更改数值方法2
            _mapDataList.Find(_ => _.name.Contains("北京")).value = "183";
            _mapDataList.Find(_ => _.name.Contains("海南")).value = "5";

            //TODO:获取标识最大值
            ViewData["maxAreaCount"] = maxAreaCount = _mapDataList.Max(_ => Convert.ToDecimal(_.value));

            //TODO:赋值透明度
            foreach (var item in _mapDataList)
            {
                item.opacity = (Convert.ToDecimal(item.value) / maxAreaCount).ToString();
            }

            //TODO:赋值Json
            ViewData["mapDataJson"] = _ser.Serialize(_mapDataList);

            return View();
        }

        /// <summary>
        /// 获取初始化地图分布
        /// </summary>
        /// <returns></returns>
        public List<YcJVectorMap.Model.Map.data> GetMapDefault(out Dictionary<string, string> mapDataDic)
        {
            List<YcJVectorMap.Model.Map.data> _mapDataList = new List<Model.Map.data>();

            mapDataDic = new Dictionary<string, string>();
            mapDataDic.Add("MAC", "澳门：");
            mapDataDic.Add("HKG", "香港：");
            mapDataDic.Add("HAI", "海南：");
            mapDataDic.Add("YUN", "云南：");
            mapDataDic.Add("BEJ", "北京：");
            mapDataDic.Add("TAJ", "天津：");
            mapDataDic.Add("XIN", "新疆：");
            mapDataDic.Add("TIB", "西藏：");
            mapDataDic.Add("QIH", "青海：");
            mapDataDic.Add("GAN", "甘肃：");
            mapDataDic.Add("NMG", "内蒙古：");
            mapDataDic.Add("NXA", "宁夏：");
            mapDataDic.Add("SHX", "山西：");
            mapDataDic.Add("LIA", "辽宁：");
            mapDataDic.Add("JIL", "吉林：");
            mapDataDic.Add("HLJ", "黑龙江：");
            mapDataDic.Add("HEB", "河北：");
            mapDataDic.Add("SHD", "山东：");
            mapDataDic.Add("HEN", "河南：");
            mapDataDic.Add("SHA", "陕西：");
            mapDataDic.Add("SCH", "四川：");
            mapDataDic.Add("CHQ", "重庆：");
            mapDataDic.Add("HUB", "湖北：");
            mapDataDic.Add("ANH", "安徽：");
            mapDataDic.Add("JSU", "江苏：");
            mapDataDic.Add("SHH", "上海：");
            mapDataDic.Add("ZHJ", "浙江：");
            mapDataDic.Add("FUJ", "福建：");
            mapDataDic.Add("TAI", "台湾：");
            mapDataDic.Add("JXI", "江西：");
            mapDataDic.Add("HUN", "湖南：");
            mapDataDic.Add("GUI", "贵州：");
            mapDataDic.Add("GXI", "广西：");
            mapDataDic.Add("GUD", "广东：");

            foreach (var item in mapDataDic)
            {
                _mapDataList.Add(new YcJVectorMap.Model.Map.data()
                {
                    id = item.Key,
                    name = item.Value,
                    value = "0",
                    url = "#"
                });
            }

            return _mapDataList;
        }
    }

演示效果如下：
![输入图片说明](http://images2015.cnblogs.com/blog/457132/201509/457132-20150916175032523-1970879481.png "在这里输入图片标题")



github:

https://github.com/cheng5x/YcJVectorMap

 

oschina.net：

http://git.oschina.net/cheng5x/YcJVectorMap